package com.choucair.formacion.definition;

import com.choucair.formacion.steps.AbrirPaginasSteps;
import com.choucair.formacion.steps.IngresarALosPrimerosSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PaginasAmarillasDefinition {

	@Steps
	AbrirPaginasSteps abrirPaginas;
	@Steps
	IngresarALosPrimerosSteps ingresarALosPrimeros;
	
	
	@Given("^que quiero realizar la consulta de diez (.*)$")
	public void que_quiero_realizar_la_consulta_de_bancos(String DeBancos) throws Throwable {
		abrirPaginas.amarillasYHacerConsulta(DeBancos);
	}

	@When("^se despliega la información de estos$")
	public void se_despliega_la_información_de_estos() throws Throwable {
		ingresarALosPrimeros.Bancos();
	}

	@Then("^Verifico que el link de la pagina web lleve al Banco Correspondiente$")
	public void verifico_que_el_link_de_la_pagina_web_lleve_al_Banco_Correspondiente() throws Throwable {
	}
}
