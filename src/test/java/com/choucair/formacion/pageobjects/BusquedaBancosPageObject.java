package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BusquedaBancosPageObject extends PageObject{
	
	//Campo de búsqueda
	@FindBy (id="keyword")
	public WebElementFacade txtBusqueda;
	
	public void primerosDiez(String palabra) {
		txtBusqueda.click();
		txtBusqueda.sendKeys(palabra+"\n");
	}

}
