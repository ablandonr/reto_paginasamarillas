package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class IngresarALaWebDelBancoPageObject extends PageObject{
	
	//Primer enlace
	@FindBy (xpath="//*[@title=\'http://www.portal80.com.co\']")
	public WebElementFacade btnlink1;
	
	//Segundo enlace
	@FindBy (xpath="//*[@title=\'http://www.financieracomultrasan.com.co\']")
	public WebElementFacade btnlink2;
	
	//Tercer enlace
	@FindBy (xpath="//*[@title=\'http://www.caminoreal.com.co\']")
	public WebElementFacade btnlink3;
	
	//Cuarto enlace
	@FindBy (xpath="//*[@title=\'http://www.banrep.gov.co\']")
	public WebElementFacade btnlink4;
	
	//Quinto enlace
	@FindBy (xpath="//*[@title=\'http://www.bancow.com.co\']")
	public WebElementFacade btnlink5;
	
	//Sexto enlace
	@FindBy (xpath="//*[@title=\'http://www.gfcobranzasjuridicas.com.co\']")
	public WebElementFacade btnlink6;
	
	//Septimo enlace
	@FindBy (xpath="//*[@title=\'http://www.grupobancolombia.com\']")
	public WebElementFacade btnlink7;
	
	//Octavo enlace
	@FindBy (xpath="//*[@title=\'http://www.dogman.com.co\']")
	public WebElementFacade btnlink8;
	
	//Noveno enlace
	@FindBy (xpath="//*[@title=\'http://www.grupogersas.com\']")
	public WebElementFacade btnlink9;
	
	//Decimo enlace
	@FindBy (xpath="//*[@title=\'http://www.cobelen.com.co\']")
	public WebElementFacade btnlink10;
		
	//Lista de Bancos
	@FindBy (xpath="/html/body/div[1]/div[2]/div/div[2]/div[2]")
	public WebElementFacade lstBanco;
	
	public void primero() {
		btnlink1.click();
	}
	public void segundo() {
		btnlink2.click();
	}
	public void tercero() {
		btnlink3.click();
	}
	public void cuarto() {
		btnlink4.click();
	}
	public void quinto() {
		btnlink5.click();
	}
	public void sexto() {
		btnlink6.click();
	}
	public void septimo() {
		btnlink7.click();
	}
	public void octavo() {
		btnlink8.click();
	}
	public void noveno() {
		btnlink9.click();
	}
	public void decimo() {
		btnlink10.click();
	}
}
