package com.choucair.formacion.steps;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

import com.choucair.formacion.pageobjects.IngresarALaWebDelBancoPageObject;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.Step;

public class IngresarALosPrimerosSteps {

	String webDelBanco;
	IngresarALaWebDelBancoPageObject ingresarALaWebDelBancoPageObject;
	
	@Step
	public void Bancos() throws InterruptedException {
		ingresarALaWebDelBancoPageObject.primero();
		switchWindow(2);
		Thread.sleep(10000);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.segundo();
		Thread.sleep(10000);
		switchWindow(3);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.tercero();
		Thread.sleep(10000);
		switchWindow(4);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.cuarto();
		Thread.sleep(10000);
		switchWindow(5);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.quinto();
		Thread.sleep(10000);
		switchWindow(6);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.sexto();
		Thread.sleep(10000);
		switchWindow(7);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.septimo();
		Thread.sleep(10000);
		switchWindow(8);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.octavo();
		Thread.sleep(10000);
		switchWindow(9);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.noveno();
		Thread.sleep(10000);
		switchWindow(10);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		ingresarALaWebDelBancoPageObject.decimo();
		Thread.sleep(10000);
		switchWindow(11);
		webDelBanco = ingresarALaWebDelBancoPageObject.getDriver().getCurrentUrl(); 
		switchWindow(1);
		List<WebElement> Lista_Url = ingresarALaWebDelBancoPageObject.lstBanco.findElements(By.tagName("a"));
		for(int i=0;i<10;i++) {
			String info = Lista_Url.get(i).getText();
			System.out.println(info);
		}
	}
	
	private static void switchWindow(int intNroWin) {
		Set<String> allWindows = Serenity.getWebdriverManager().getCurrentDriver().getWindowHandles();
		Integer intW = 0;
		for(String curWindow : allWindows) {
			intW = intW + 1;
			if (intW == intNroWin) {
				Serenity.getWebdriverManager().getCurrentDriver().switchTo().window(curWindow);
				break;
			}
		}
	}

}
