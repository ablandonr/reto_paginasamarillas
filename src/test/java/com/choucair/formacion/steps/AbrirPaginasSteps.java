package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.BusquedaBancosPageObject;
import com.choucair.formacion.pageobjects.PaginasAmarillasWebPageObject;

import net.thucydides.core.annotations.Step;

public class AbrirPaginasSteps {
	
	PaginasAmarillasWebPageObject paginasAmarillasWeb;
	BusquedaBancosPageObject busquedaBancos;

	@Step
	public void amarillasYHacerConsulta(String Bancos) throws InterruptedException {
		paginasAmarillasWeb.open();
		Thread.sleep(3000);
		busquedaBancos.primerosDiez(Bancos);
		Thread.sleep(3000);
		
	}

	
}
